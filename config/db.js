const mongoose = require('mongoose')

const connectDB = async () => {
    try{
        mongoose.connect("mongodb+srv://admin:admin123@zuitt.qfnw1x1.mongodb.net/shop-mart?retryWrites=true&w=majority", {
            useNewUrlParser: true,
            UseUnifiedTopology: true
        })

        mongoose.connection.once('open', () => console.log("Now Connected to MongoDB"))
    } catch(error) {
        console.log(error)
        process.exit(1)
    } 
}

module.exports = connectDB
