const Product = require('../models/Product.js')

// Create product data 
const createProduct = async (data) => {

   if (data.isAdmin) {

      const { categories, brand, name, screenSize, cpu, videoCard, ram, storage, OS, description, price, source } = data.product
      const newProduct = new Product({
         categories: categories,
         brand: brand,
         name: name,
         screenSize: screenSize,
         cpu: cpu,
         videoCard: videoCard,
         ram: ram,
         storage: storage,
         OS: OS,
         description: description,
         source: source,
         price: Number(price),
      })

      const fields = Object.keys(data.product)
      // const fieldEntry = Object.entries(reqBody)
      const allowedProperty = ['categories', 'brand', 'name', 'screenSize', 'cpu', 'videoCard', 'ram', 'storage', 'OS', 'description', 'source', 'price']
      // Check if the reqBody Fieldname is correct if not return invalid Field names
      const checkFieldValidators = fields.every(update => allowedProperty.includes(update))
      // Check the reqBody missing object property
      const missingFields = allowedProperty.filter(item => fields.indexOf(item) == -1)

      if (fields.length < allowedProperty.length) {
         return `${missingFields.toString()} Field is required!`
      }


      if (!checkFieldValidators) {
         return {
            message: `Field name is invalid!`
         }
      }





      if (screenSize.length == 0 || cpu.length == 0 ||
         videoCard.length == 0 || ram.length == 0 ||
         storage.length == 0 || OS.length == 0) {
         return 'The Specs is must be complete!'
      }

      if (price.length == 0) {
         return 'The price value is invalid'
      }

      if (categories.length == 0) {
         return 'The categories value is invalid'
      }

      if (!Number.isInteger(price)) {
         return 'The Price must be a number!'
      }



      const result = await newProduct.save()

      if (!result) {
         return "The product is not created"
      }

      return {
         message: "Product is successfully created",
         product_Specs: {
            name: name,
            brand: brand,
            screenSize: screenSize,
            cpu: cpu,
            videoCard: videoCard,
            ram: ram,
            storage: storage,
            OS: OS,
            categories: categories,
            description: description,
            source: source,
            price: price,
         }
      }

   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }
}

// Read active product data 
const getActiveProduct = async (qNew, qCategory) => {
   if (qNew) {
      let products = await Product.find({ isActive: true }).sort({ createdAt: -1 }).limit(1)
      return products
   } else if (qCategory) {
      let products = await Product.find({
         isActive: true,
         categories: {
            $in: [qCategory],
         },
      })
      return products
   } else {
      let products = await Product.find({ isActive: true });
      return products
   }
}

// Read specific product data
const getSpecificProduct = async (productId) => {
   const result = await Product.findById(productId)

   if (!result) {
      return "Nothing Found"
   }

   return result
}

const updateProduct = async (data, reqBody) => {

   if (data.isAdmin) {

      const updateProduct = await Product.findByIdAndUpdate(data.productId, reqBody)


      if (reqBody.price !== undefined) {
         if (!Number.isInteger(reqBody.price)) {
            return 'The price must be a number'
         }
      }


      if (!updateProduct) {
         return 'the update product page is under maintenance'
      }

      return {
         message: `Updated Sucessfully`,
         product: reqBody
      }

   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }
}

const archiveProduct = async (data) => {
   if (data.isAdmin) {

      const result = await Product.findByIdAndUpdate(data.productId, {
         isActive: false
      })

      if (!result) {
         return 'nothing found'
      }

      return {
         message: "updated successfully",
         product: result
      }




   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }

}

const unArchiveProduct = async (data) => {
   if (data.isAdmin) {

      const result = await Product.findByIdAndUpdate(data.productId, {
         isActive: true
      })

      if (!result) {
         return 'nothing found'
      }

      return {
         message: "updated successfully",
         product: result
      }




   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }

}

const allProducts = async (data) => {
   if (data.isAdmin) {

      if (data.qNew) {
         let products = await Product.find().sort({ createdAt: -1 }).limit(1)
         return products
      } else if (data.qCategory) {
         let products = await Product.find({
            categories: {
               $in: [data.qCategory],
            },
         })
         return products
      } else {
         let products = await Product.find();
         return products
      }


   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }
}

const deleteProduct = async (data, productId) => {
   if (data.isAdmin) {
      const result = await Product.findByIdAndDelete(productId);

      if (!result) {
         return 'Nothing Found!'
      }

      return {
         message: `Delete Sucessfully`,
         product: result
      }

   } else {
      return {
         message: 'User is not allowed to access in this page'
      }
   }

}


module.exports = {
   createProduct,
   getActiveProduct,
   getSpecificProduct,
   updateProduct,
   archiveProduct,
   deleteProduct,
   allProducts,
   unArchiveProduct
}
