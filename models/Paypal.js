const mongoose = require('mongoose')

const paypalSchema = new mongoose.Schema({


      userId: {
        type: String,
        trim: true,
        required: [true, 'User Id is required!']
      },

      firstName: {
        type: String,
        trim: true,
        required: [true, 'Firstname is required!']
      },
  
      lastName: {
        type: String,
        trim: true,
        required: [true, 'Lastname is required!']
      },
  
      email: {
        type: String,
        trim: true,
        required: [true, 'Email is required!']
      },
  
      mobileNo: {
        type: String,
        trim: true,
        required: [true, 'Mobile number is required!']
      },
  
      address: {
        type: 'String',
        trim: true,
        require: [true, 'Address is reqiired!']
      },
 

    creditCard:{
      type: String,
      require: [true, 'Credit card is required']
    },

    cardExpDate: {
      type: String,
      required: [true, 'Card expiration date is required']
    },

    cardNumber: {
      type: String,
      trim: true,
      required: [true, 'Card number is required!']
    },

    securityCode: {
      type: String,
      trim: true,
      required: [true, 'Card number is required!']
    },

    amount: {
      type: Number,
      default: 5000000
    },

    orders:[{}],

    addtocart:[],

    createdOn: {
      type: Date,
      default: new Date()
    }

})

module.exports = mongoose.model('paypalCard', paypalSchema)