const mongoose = require('mongoose')


const checkAll = new mongoose.Schema({
    userId: {
      type: String,
      trim: true,
      required: [true, 'User Id is required']
    },

    cart: [],

    isPaid: {
      type: Boolean,
      default: false
    },

    status: {
       type: String,
       default: "pending",
    },

     createdOn: {
        type: Date,
        default: new Date()
     } 

})


module.exports = mongoose.model('Checkall', checkAll)