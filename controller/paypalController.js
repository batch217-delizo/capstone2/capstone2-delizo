const User = require('../models/User.js')
const Paypal = require('../models/Paypal.js')


const createUserCard = async (data, reqBody) => {
    if(data.isAdmin) {
      return {
               message: 'Admin is not allowed to access in this page'
             }
    } else {
      
            const  {creditCard, cardExpDate, cardNumber, securityCode }  = reqBody
            const userData = await User.findById(data.userId)
            const fields =  Object.keys(reqBody)
            // const fieldEntry = Object.entries(reqBody)
            const allowedProperty = [ 'creditCard', 'cardExpDate','cardNumber', 'securityCode' ]
            // Check if the reqBody Fieldname is correct if not return invalid Field names
            const checkFieldValidators =  fields.every( update =>  allowedProperty.includes(update) )
            // Check the reqBody missing object property
            const missingFields = allowedProperty.filter(item => fields.indexOf(item) == -1);

            const cardNumberRecord = await Paypal.find({cardNumber: cardNumber});

            const securityCodeRecord = await Paypal.find({securityCode: securityCode});

            if(fields.length < allowedProperty.length) {
               return {
                  message: `${missingFields.toString()} Field is required!` 
               }  
            }

           if(!checkFieldValidators) {
            return  {
                      message : `Field name is invalid!`
                     }
          }

            if(cardExpDate.length != 4){
                  return {
                     message: 'credicard expiration date value is invalid'
                  }
            }

            if(cardExpDate > '1230' && cardExpDate < '1225' ) {
               return {
                  message: 'credicard expiration date defaul must 1230'
               }
            }

           
            if(cardNumber.length != 19){
               return {
                  message: `Card number lenght value is invalid!`
               }
            } 

            if(securityCode.length != 4){
               return {
                  message: `Card number lenght value is invalid!`
               }
            } 


            if(cardNumberRecord.length >= 1){
               return {
                  message: `The card number already used`
               }
            } 
 

            if(cardNumberRecord.length >= 1){
               return {
                  message: `The card number already used`
               }
            } 


            if(securityCodeRecord.length >= 1){
               return {
                  message: `The security code is already used`
               }
            } 

         
       

            if(  creditCard.length == 0 || cardExpDate.length == 0 || 
                 cardNumber.length == 0 || securityCode.length == 0 ) {
        
               return {
                 message: "Please complete the registration form"  
               } 
           }

           if(creditCard.toLowerCase() == 'visa' || creditCard.toLowerCase() == 'american express' || creditCard.toLowerCase() == 'master card') {
         
               const registerPaypal = new Paypal({
                
                     userId: userData._id,
                     firstName: userData.firstName,
                     lastName: userData.lastName,
                     email: userData.email,
                     mobileNo: userData.mobileNo,
                     address: userData.address,
                
                  creditCard: creditCard.toLowerCase(),
                  cardExpDate: cardExpDate,
                  cardNumber: cardNumber,
                  securityCode: securityCode
            })

             const paypalRegistration =  await registerPaypal.save() 
      
               if(!paypalRegistration) {
                  return "Registration Failed" 
               }
     
            return {
                       message: ` Congratulations, ${userData.firstName} ${ userData.firstName}  your papal account has been successfully created.`,
                       result: paypalRegistration 
               }
         }  
          
            
           
          }
}


const readProfile = async (data) => {

   if(data.isAdmin) {
      return {
               message: 'Admin is not allowed to access in this page'
             }
    } else {
      const result = await Paypal.find({userId: data.userId})

      if(!result) {
           return {
               message: `No user found!`,
           } 
      }

      result.forEach(item => {
         item.orders = undefined
         item.addtocart = undefined
      })
      
      return {
         message: "User found",
         result: result
      }
   }
}


// Admin monitoring

const adminMonitoring = async (data) => {
   if(data.isAdmin) {

      const result = await Paypal.find()

      if(!result) {
         return {
             message: `No user found!`,
         } 
    }

      if(result == []) {
         return {
                   message: `No user found!`,
                  } 
      }

      result.forEach( item => {
            item.userId = undefined
            item.orders.forEach( data => {
               data.userId = undefined
               data.firstName = undefined
               data.lastName = undefined
            })
      })

    return {
       message: "User found",
       result: result
    }
   
    } else {
     

      return {
         message: 'User is not allowed to access in this page'
       }
   }
}

module.exports = {
   createUserCard,
   readProfile,
   adminMonitoring
}


