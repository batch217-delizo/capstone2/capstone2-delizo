const express = require('express')
const router = express.Router()
const auth = require('../middleware/authMiddleware.js')
const { createUserCard, readProfile, adminMonitoring } = require('../controller/paypalController.js')


// Create user payment card
router.post('/create', auth.verify ,(req, res) => {
    const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      userId: auth.decode(req.headers.authorization).id
    }
    
    createUserCard(data, req.body).then( 
        resultFromContainer => res.send(resultFromContainer))
})

// show user creditCard profile
router.get('/profile/me', auth.verify, (req, res) => {
  const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
     userId: auth.decode(req.headers.authorization).id
  }
  readProfile(data,req.params.userId).then( 
      resultFromContainer => res.send(resultFromContainer))
})

router.get('/profile/me', auth.verify, (req, res) => {
  const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
     userId: auth.decode(req.headers.authorization).id
  }
  readProfile(data,req.params.userId).then( 
      resultFromContainer => res.send(resultFromContainer))
})

router.get('/monitoring', auth.verify, (req, res) => {
  const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
  }
  
  adminMonitoring(data).then( 
      resultFromContainer => res.send(resultFromContainer))
})

router.get('/monitoring/cart', auth.verify, (req, res) => {
  const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
  }
  
adminMonitoring(data).then( 
      resultFromContainer => res.send(resultFromContainer))
})


module.exports = router