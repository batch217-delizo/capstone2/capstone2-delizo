const express = require('express')
const cors = require('cors')
const colors = require('colors')
const connectDB = require('./config/db.js')

connectDB()

const app = express()

/* Middleware */
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

/* Routes */
app.use('/users', require('./routes/users.js'))
app.use('/products', require('./routes/product.js'))
app.use('/orders', require('./routes/order.js'))
app.use('/carts', require('./routes/cart.js'))
app.use('/paymentCard', require('./routes/paypal.js'))
app.use('/checkAll', require('./routes/checkAll.js'))
const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`API is now online at Port: ${port}`))

