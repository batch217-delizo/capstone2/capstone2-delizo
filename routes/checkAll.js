const express = require('express')
const router = express.Router()
const auth = require('../middleware/authMiddleware.js')
const { readyToPay, readReadyToPayCart, payCartOrder, getAllCheckOut } = require('../controller/checkAllController.js')

router.post('/cart/active', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.body.cartId
   }


   readyToPay(data).then(
      resultFromController => res.send(resultFromController))
})


router.get('/cart/all', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      orderId: req.body.orderId,
      cardNumber: req.body.cardNumber,
      cardType: req.body.cardType,
      cardExpDate: req.body.cardExpDate,
      securityCode: req.body.securityCode
   }


   readReadyToPayCart(data).then(
      resultFromController => res.send(resultFromController))
})


router.get('/all', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
   }


   getAllCheckOut(data).then(
      resultFromController => res.send(resultFromController))
})


router.patch('/cart/payNow', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.body.cartId,
      cardNumber: req.body.cardNumber,
      creditCard: req.body.creditCard,
      cardExpDate: req.body.cardExpDate,
      securityCode: req.body.securityCode
   }


   payCartOrder(data).then(
      resultFromController => res.send(resultFromController))
})


module.exports = router