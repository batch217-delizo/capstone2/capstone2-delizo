const Checkall = require('../models/Checkall.js')
const Cart = require('../models/Cart.js')
const Paypal = require('../models/Paypal.js')

const readyToPay = async (data) => {

  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const cartData = await Cart.findById(data.cartId)



    const checkAll = new Checkall({
      userId: data.userId,
      cart: [cartData],
    })

    const checkAllResult = checkAll.save()

    if (!checkAllResult) {
      return {
        message: 'cart not check failed'
      }
    }

    const result = await Cart.findByIdAndUpdate(data.cartId, {
      isActive: true
    })


    return {
      messasge: 'cart is checked ready to pay'
    }
  }
}

const readReadyToPayCart = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const result = await Checkall.find({ userId: data.userId })

    if (!result) {
      return {
        message: 'Nothing Found'
      }
    }

    if (result.length == []) {
      return {
        message: 'Nothing Found'
      }
    }

    return result

  }

}

const getAllCheckOut = async (data) => {
  if (data.isAdmin) {

    const result = await Checkall.find()

    if (!result) {
      return {
        message: 'Nothing Found'
      }
    }

    if (result.length == []) {
      return {
        message: 'Nothing Found'
      }
    }

    return result

  } else {

    return {
      message: 'User is not allowed to access in this page'
    }

  }

}


const payCartOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const creditCard = await Paypal.findOne({ creditCard: data.creditCard })
    const expire_date = await Paypal.findOne({ cardExpDate: data.cardExpDate })
    const security_code = await Paypal.findOne({ securityCode: data.securityCode })
    const payPal_id = await Paypal.findOne({ cardNumber: data.cardNumber })
    const cartResult = await Checkall.findById(data.cartId)

    console.log(cartResult._id + "ito ang data na nakuha")


    if (!cartResult) {
      return `Cart Id Nothing Found`
    }

    if (typeof cartResult == "undefined") {
      return `Cart Id Nothing Found`
    }

    const emptyCartAmountArray = []


    cartResult.cart.forEach(element => {
      emptyCartAmountArray.push(element.Subtotal)
    });



    if (!creditCard) {
      return 'Credit Card type not found'
    }

    if (!payPal_id) {
      return 'Card number not found'
    }

    if (!security_code) {
      return 'Security code not found'
    }

    if (!expire_date) {
      return 'Expiry date not found'
    }

    const isUserCardUpdated = await Paypal.findById(payPal_id.id).then(data => {

      data.addtocart.push(cartResult)

      if (emptyCartAmountArray[0] > data.amount) {
        return false
      }

      const subtract = data.amount - emptyCartAmountArray[0]
      data.amount = subtract;

      return data.save().then((user, error) => {
        if (error) {
          return false
        }
        return true
      });
    })



    const isUserCartUpdated = await Checkall.findById(cartResult).then(orderData => {
      if (orderData.isPaid == true) {
        return false
      }

      const cartTotalAmount = orderData.cart.map(item => {
        return item.Subtotal
      }
      )

      if (cartTotalAmount > payPal_id.amount) {

        return false
      }

      orderData.status = 'paid'
      orderData.isPaid = true
      return orderData.save().then((user, error) => {
        if (error) {
          return false
        }
        return true
      });
    })




    cartResult.userId = undefined


    let mytotalAmount
    let cartArrayList = []

    cartResult.cart.forEach(element => {
      element.userId = undefined
      element.products.forEach(item => {
        item.productId = undefined
        item.userId = undefined
        cartArrayList.push(item)
        mytotalAmount = item.totalAmount
      })

    })

    if (isUserCardUpdated && isUserCartUpdated) {
      return {
        message: `Thank you ${cartResult.cart[0].firstName} ${cartResult.cart[0].lastName} `,
        product: cartResult.products,
        TotalAmount: ` ${mytotalAmount}`,

      }

    } else {
      return false;
    }




  }

}

module.exports = {
  readyToPay,
  readReadyToPayCart,
  payCartOrder,
  getAllCheckOut
}