const Product = require('../models/Product.js')
const Cart = require('../models/Cart.js');
const User = require('../models/User');


// add to cart
const addToCart = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const userData = await User.findById(data.userId);
    const product = await Product.findById(data.productId);

    if (!product) {
      return {
        message: `Nothing found`
      }
    }

    const newCart = new Cart({
      userId: data.userId,
      firstName: userData.firstName,
      lastName: userData.lastName,
      products: [{
        productId: product._id,
        productBrand: product.brand,
        productCategories: product.categories,
        productName: product.name,
        productScreenSize: product.screenSize,
        productCPU: product.cpu,
        productVideoCard: product.videoCard,
        productRam: product.ram,
        productStorage: product.storage,
        productOS: product.OS,
        productPrice: product.price,
        quantity: data.quantity,
        totalAmount: data.quantity * product.price
      }],
      Subtotal: data.quantity * product.price
    })

    if (!Number.isInteger(data.quantity)) {
      return {
        message: `Quantity must be a number!`
      }
    }

    if (data.quantity <= 0) {
      return {
        message: `the minimun value is 1`
      }
    }

    if (data.quantity > 10) {
      return {
        message: `the maximum value is 10`
      }
    }

    let cartId = await Cart.find({ userId: data.userId })

    let getIsPaidToFalse = cartId.map(element => {
      if (element.isActive == false) {
        return element.id
      }
    })

    let cartData = await Cart.findById(getIsPaidToFalse).then(myCart => {

      if (!myCart) {
        return false
      }

      if (myCart.isActive == true) {
        return false
      }

      myCart.products.push({
        productId: product._id,
        productBrand: product.brand,
        productCategories: product.categories,
        productName: product.name,
        productScreenSize: product.screenSize,
        productCPU: product.cpu,
        productVideoCard: product.videoCard,
        productRam: product.ram,
        productStorage: product.storage,
        productOS: product.OS,
        productPrice: product.price,
        quantity: data.quantity,
        totalAmount: data.quantity * product.price
      })

      let numberArray = []

      myCart.products.map(product => { return numberArray.push(product.totalAmount) })

      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)

      console.log(sum)

      return myCart.save().then((user, error) => {
        if (error) {
          return false
        } else {

          return Cart.findByIdAndUpdate(getIsPaidToFalse,
            { Subtotal: sum }).then((user, error) => {
              if (error) {
                return false
              } else {
                return true
              }
            })
        }
      })
    })

    console.log(cartData)





    if (cartData == false || cartId.length == []) {
      console.log(cartData)
      const resultSave = await newCart.save();
      console.log(resultSave)
      return {
        message: `Product successfully added to your shopping cart`,
      }
    }

    if (cartData == true) {
      return {
        message: `Product successfully added to your shopping cart`,
      }
    }



  }
}

// Check Specific Cart 

const specificCart = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {

    const result = await Cart.findById(data.cartId)

    if (!result) {
      return {
        message: 'Nothing Found!'
      }

    }

    if (result.length == []) {
      return 'Nothing Found!'
    }

    result.userId = undefined;
    result.products.forEach(element => {
      element.productId = undefined

    })

    return result
  }
}


// Read user cart
const userCart = async (data) => {
  console.log(data.isAdmin + " data code")
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {


    let result = await Cart.find({ userId: data.userId })


    if (!result) {
      return 'Nothing Found'
    }

    if (result.length == []) {
      return 'Nothing Found'
    }


    result.forEach(element => {
      element.userId = undefined;
      element.products.forEach(item => {
        item.productId = undefined;
      })
    })

    return result
  }
}

const showAllCarts = async (data) => {
  if (data.isAdmin) {
    let result = await Cart.find({})

    if (!result) {
      return 'Nothing Found'
    }

    if (result.length == []) {
      return 'Nothing Found'
    }

    result.forEach(element => {
      element.userId = undefined;
      element.products.forEach(item => {
        item.productId = undefined;
      })
    })

    console.log("show all carts")

    return result
  } else {

    return {
      message: 'User is not allowed to access in this page'
    }

  }
}

// Edit cart product
const editUserCart = async (data) => {
  console.log(data)
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    /*     const result = await Cart.findById(data.cartId)
    
        console.log(result + " ito ang resulta ::   : : : : : : : ")
    
    
        if (result.isActive == true) {
          return {
            message: ` your order is now processed you have no authorized to edit it!`
          }
        }
    
        if (!result) {
          return {
            message: 'No user found!'
          }
        }
    
        if (!Number.isInteger(data.quantity)) {
          return {
            message: `Quantity must be a number!`
          }
        } */

    if (data.quantity <= 0) {
      return {
        message: `the minimun value is 1`
      }
    }

    if (data.quantity > 10) {
      return {
        message: `the maximum value is 10`
      }
    }

    const cartResult = await Cart.findById(data.cartId).then(myCart => {
      console.log("data of my cart" + myCart)
      myCart.products.filter(product => {

        console.log(product.id.toString() + "myProduct")
        if (product._id == data.productId) {
          product.quantity = data.quantity,
            product.totalAmount = data.quantity * product.productPrice
          return product;

        }
      })

      let numberArray = []

      myCart.products.map(product => { return numberArray.push(product.totalAmount) })

      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)

      console.log('myCartnow' + myCart + ` is this now`)
      return myCart.save().then((user, error) => {
        if (error) {
          return false
        } else {

          return Cart.findByIdAndUpdate(data.cartId,
            { Subtotal: sum }).then((user, error) => {
              if (error) {
                return false
              } else {
                return true
              }
            })
        }
      })
    });




    if (cartResult == true) {
      return {
        message: `Product successfully updated to your shopping cart`
      }
    } else {
      return {
        message: `Product failed updated to your shopping cart`
      }
    }

  }
}

const deleteCart = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const result = await Cart.findByIdAndDelete(data.cartId)

    if (!result) {
      return false
    }

    return true
  }
}


const deleteUserCart = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const result = await Cart.findById(data.cartId)
    console.log(result + "my result")
    if (!result) {

      return {
        message: `No product found!`
      }
    }

    if (result.products.length == 0) {

      const deleteWholeOrder = await Cart.findByIdAndDelete(data.cartId)
    }



    const deletedOrderCart = await Cart.findById(result._id).then(myCart => {


      if (myCart.isActive == true) {
        return false
      }

      const myData = myCart.products.findIndex(item => item.id === data.productId)

      if (myData == -1) {
        return {
          message: 'No result found'
        }
      }

      myCart.products.splice(myData, 1)

      let numberArray = []

      console.log(" please read my array" + numberArray);




      myCart.products.map(product => { return numberArray.push(product.totalAmount) })


      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)


      return myCart.save().then((user, error) => {
        if (error) {
          return false
        } else {

          return Cart.findByIdAndUpdate(data.cartId,
            { Subtotal: sum }).then((user, error) => {
              if (error) {
                return false
              } else {
                return true
              }
            })
        }
      })
    });



    if (deletedOrderCart == true) {
      return {
        message: `Product successfully deleted to your shopping cart`,
      }
    } else {
      return {
        message: `Product failed deleted to your shopping cart`,
      }
    }

  }

}

module.exports = {
  addToCart,
  userCart,
  editUserCart,
  deleteUserCart,
  showAllCarts,
  specificCart,
  deleteCart
}